package ch.tbz.alp.resttest.controller;

import ch.tbz.alp.resttest.entities.PersonCount;
import ch.tbz.alp.resttest.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController {

    @Autowired
    private PersonRepository personRepository;

    @GetMapping("/count")
    public PersonCount personCount() {
        return new PersonCount((int) personRepository.count());
    }

}
