package ch.tbz.alp.resttest.entities;


import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class PersonCount {

    public PersonCount() {

    }

    public PersonCount(int count) {
        this.count = count;
    }

    public int count;

    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    @Id
    public Long getId() {
        return id;
    }
}
