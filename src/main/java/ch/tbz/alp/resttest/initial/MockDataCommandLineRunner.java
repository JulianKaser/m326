package ch.tbz.alp.resttest.initial;

import ch.tbz.alp.resttest.entities.Person;
import ch.tbz.alp.resttest.repositories.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class MockDataCommandLineRunner implements CommandLineRunner {

    @Autowired
    public PersonRepository personRepository;

    @Override
    public void run(String... args) throws Exception {

        Person p1 = new Person();
        p1.setFirstName("Philipp");
        p1.setLastName("Albrecht");
        personRepository.save(p1);

        Person p2 = new Person();
        p2.setFirstName("Max");
        p2.setLastName("Müller");
        personRepository.save(p2);

    }
}
